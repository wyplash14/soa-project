import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'soa-mfe';

  countdownDate: string = '';
  remainingTime: string = '';

  startCountdown(): void {
    const endDate = new Date(this.countdownDate).getTime();

    if (isNaN(endDate)) {
      alert('Please select a valid date.');
      return;
    }

    const interval = setInterval(() => {
      const now = new Date().getTime();
      const distance = endDate - now;

      if (distance < 0) {
        clearInterval(interval);
        this.remainingTime = 'EXPIRED';
        return;
      }

      const days = Math.floor(distance / (1000 * 60 * 60 * 24));
      const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      this.remainingTime = `${days}d ${hours}h ${minutes}m ${seconds}s`;

    }, 1000);
  }
}
