import {CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import {LazyElementsModule} from "@angular-extensions/elements";
import {createCustomElement} from "@angular/elements"

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    LazyElementsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private injector:Injector) {
  }

  ngDoBootstrap() {
    const element = createCustomElement(AppComponent, {injector: this.injector})
    customElements.define('app-micro-fe', element)
  }
}
