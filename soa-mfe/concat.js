const concat = require("concat");
(async function build() {
  const files = [
    "./dist/soa-mfe/runtime.js",
    "./dist/soa-mfe/polyfills.js",
    "./dist/soa-mfe/main.js",
  ];
  await concat(files, "./dist/micro-fe/micro-fe.js");
})();
