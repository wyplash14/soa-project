import { Data, Route } from '@angular/router';

export type NgRouteData = Data & {
  // this might come in handy
};

export type NgRoute = Route & {
  data?: NgRouteData;
};

export type NgRoutes = Array<Route & NgRoute>;
