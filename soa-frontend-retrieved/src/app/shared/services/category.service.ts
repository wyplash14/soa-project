import {Injectable} from '@angular/core';
import {BackendService} from "./backend.service";
import {Observable} from "rxjs";
import {Category} from "../../models/category-model";
import {SubCategory} from "../../models/subCategory-model";
import {Item} from "../../models/item-model";
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/dynamicdialog";

const API_BASE = 'http://localhost:8083/soa-project/api';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private service: BackendService) {
  }

  getCategories(): Observable<Category[]> {
    return this.service.get(API_BASE + '/categories');
  }

  getSubcategories(categoryId: Number): Observable<SubCategory[]> {
    return this.service.get(API_BASE + '/subcategories/category/' + categoryId)
  }

  getItems(): Observable<Item[]> {
    return this.service.get(API_BASE + '/items');
  }

  getItemById(selectedItemId: number): Observable<Item> {
    return this.service.get(API_BASE + '/items/' + selectedItemId)
  }

  createItem(dialogConfig:DynamicDialogConfig):Observable<Item> {
    const body = {
      name: dialogConfig.data.name,
      estimatedValue: dialogConfig.data.estimatedValue,
      minimalValue: dialogConfig.data.minimalValue,
      actualBid: dialogConfig.data.minimalValue,
      endOfAuction:dialogConfig.data.selectedDate,
      description:dialogConfig.data.description,
      sellerId:1,
      subCategoryId:dialogConfig.data.subCategory.id
    };
    console.log(body)
    return this.service.post(API_BASE + '/items', body)
  }
}
