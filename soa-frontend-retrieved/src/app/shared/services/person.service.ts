import {Injectable} from '@angular/core';
import {BackendService} from "./backend.service";
import {Observable} from "rxjs";
import {Bids} from "../../models/bids-model";
import {Person} from "../../models/person-model";

const API_BASE = 'http://localhost:8083/soa-project-persons/api';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(private service: BackendService) {
  }

  getBidsOfItem(itemId: number): Observable<Bids[]> {
    return this.service.get(API_BASE + '/bids/' + itemId);
  }

  getPerson(personId: number): Observable<Person> {
    return this.service.get(API_BASE + '/persons/' + personId);
  }

  createBid(id: number | undefined, newBidAmount: number) {
    const body = {
      itemId: id,
      bidPrice: newBidAmount,
      personId: 3
    };
    return this.service.post(API_BASE + '/bids', body)
  }
}
