import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import {MenubarModule} from "primeng/menubar";
import {HttpClientModule} from "@angular/common/http";
import {ButtonModule} from "primeng/button";
import { HomeComponent } from './home/home.component';
import {CarouselModule} from "primeng/carousel";
import {TagModule} from "primeng/tag";
import {ProductService} from "./home/productservice";
import { SellerListComponent } from './seller-list/seller-list.component';
import {CardModule} from "primeng/card";
import { CategoriesListComponent } from './categories-list/categories-list.component';
import {MenuModule} from "primeng/menu";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { FooterComponent } from './footer/footer.component';
import { NewItemDialogComponent } from './new-item-dialog/new-item-dialog.component';
import {FormsModule} from "@angular/forms";
import {DropdownModule} from "primeng/dropdown";
import {DialogService} from "primeng/dynamicdialog";
import {InputTextModule} from "primeng/inputtext";
import {KeyFilterModule} from "primeng/keyfilter";
import {CalendarModule} from "primeng/calendar";
import {InputTextareaModule} from "primeng/inputtextarea";
import {AngularFireStorageModule} from "@angular/fire/compat/storage";
import {AngularFireModule} from "@angular/fire/compat";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {FileUploadModule} from "primeng/fileupload";
import {LazyElementsModule} from "@angular-extensions/elements";
import {KeycloakAngularModule, KeycloakService} from "keycloak-angular";

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: 'http://localhost:8080',
        realm: 'springboot-microservice-realm',
        clientId: 'Soa-Client'
      },
    });
}


@NgModule({
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    SellerListComponent,
    CategoriesListComponent,
    FooterComponent,
    NewItemDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MenubarModule,
    HttpClientModule,
    ButtonModule,
    CarouselModule,
    TagModule,
    CardModule,
    MenuModule,
    BrowserAnimationsModule,
    FormsModule,
    DropdownModule,
    InputTextModule,
    KeyFilterModule,
    CalendarModule,
    InputTextareaModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyDuIZvRyC6-wPCdcuSVqD4avEjxTSr_LXQ",
      authDomain: "soa-project-9afe1.firebaseapp.com",
      databaseURL: "https://soa-project-9afe1-default-rtdb.europe-west1.firebasedatabase.app",
      projectId: "soa-project-9afe1",
      storageBucket: "soa-project-9afe1.appspot.com",
      messagingSenderId: "663684800291",
      appId: "1:663684800291:web:51fae5f4cc7fda22973696",
      measurementId: "G-Y6917173YG"
    }),
    FileUploadModule,
    LazyElementsModule,
    KeycloakAngularModule,
  ],
  providers: [ProductService, DialogService,AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
