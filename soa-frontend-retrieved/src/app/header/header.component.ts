import {Component} from '@angular/core';
import {CategoryService} from "../shared/services/category.service";
import {MenuItem} from "primeng/api";
import {NewItemDialogComponent} from "../new-item-dialog/new-item-dialog.component";
import {DialogService} from "primeng/dynamicdialog";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  menuItems: MenuItem[] = [];

  ngOnInit(): void {
    // Fetch menu items from the backend
    this.categoryService.getCategories().subscribe((items) => {
      // Transform backend items into PrimeNG MenuItem format
      this.menuItems = items.map((item) => ({
        label: item.name,
        routerLink: ['/categories', item.id]
      }));
    });
  }

  constructor(public dialogService: DialogService,private categoryService: CategoryService) { }

  openNewItemDialog(): void {
    const ref = this.dialogService.open(NewItemDialogComponent, {
      header: 'Create New Item',
      width: '70%',
      height: '100%',
      data: {
        // any initial data if needed
      }
    });

    ref.onClose.subscribe((result) => {
      if (result) {
        // Handle the saved data or any other logic here
        console.log(result);
      }
    });
  }
}
