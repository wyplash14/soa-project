import {Component, OnInit} from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from "primeng/dynamicdialog";
import {CategoryService} from "../shared/services/category.service";
import {SubCategory} from "../models/subCategory-model";
import {Category} from "../models/category-model";
import {AngularFireStorage} from "@angular/fire/compat/storage";

@Component({
  selector: 'app-new-item-dialog',
  templateUrl: './new-item-dialog.component.html',
  styleUrls: ['./new-item-dialog.component.css']
})
export class NewItemDialogComponent implements OnInit {
  categories: Category[] = []
  subcategories: SubCategory[] = []

  path: string = ''

  constructor(
    public dialogRef: DynamicDialogRef,
    public dialogConfig: DynamicDialogConfig,
    public categoryService: CategoryService,
    private fs: AngularFireStorage,
  ) {
  }

  ngOnInit() {
    this.categoryService.getCategories().subscribe(categories => {
      this.categories = categories
    })
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): void {

    this.categoryService.createItem(this.dialogConfig).subscribe(elem => {
      console.log(elem)
      this.fs.upload("/files_item_" + elem.id + "_" + elem.name, this.path)
    })
    this.dialogRef.close();
  }

  public handleSubcategoryLoad() {
    this.categoryService.getSubcategories(this.dialogConfig.data.category.id).subscribe(subcategories => this.subcategories = subcategories)
  }

  onUpload(event:any) {
    this.path = event.target.files[0]
    console.log(this.path)
  }
}
