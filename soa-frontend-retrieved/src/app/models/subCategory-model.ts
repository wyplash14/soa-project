import {Category} from "./category-model";

export interface SubCategory {

  id: number;
  categoryOutgoingDto: Category;

  name: string;
}
