import {SubCategory} from "./subCategory-model";

export interface Item {
  id: number;
  name: string;
  estimatedValue: number;
  minimalValue: number;
  actualBid: number;
  endOfAuction: any;
  description: string;
  sellerId: number;
  subCategoryOutgoingDto: SubCategory;
  imageUrl: string;
}
