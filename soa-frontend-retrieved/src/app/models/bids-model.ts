import {Person} from "./person-model";

export interface Bids {
  itemId: number;
  personOutgoingDto: Person;
  bidPrice: number;
}
