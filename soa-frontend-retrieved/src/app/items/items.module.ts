import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ItemsComponent} from './items/items.component';
import {NgRoutes} from "../shared/types";
import {RouterModule} from "@angular/router";
import {MenubarModule} from "primeng/menubar";
import {ItemBidComponent} from './item-bid/item-bid.component';
import {CardModule} from "primeng/card";
import { ItemDetailDialogComponent } from './item-detail-dialog/item-detail-dialog.component';
import {DialogModule} from "primeng/dialog";
import {FormsModule} from "@angular/forms";
import {ImageModule} from "primeng/image";
import {LazyElementsModule} from "@angular-extensions/elements";


const managementRoutes: NgRoutes = [
  {path: '', component: ItemsComponent,
    children: [
      {
        path: 'subcategories/:id',
        component: ItemBidComponent
      }
    ]
  },

];

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    ItemsComponent,
    ItemBidComponent,
    ItemDetailDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(managementRoutes),
    MenubarModule,
    CardModule,
    DialogModule,
    FormsModule,
    ImageModule,
    LazyElementsModule,
  ],
})
export class ItemsModule {
}
