import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Item} from "../../models/item-model";
import {PersonService} from "../../shared/services/person.service";
import {Bids} from "../../models/bids-model";
import {Person} from "../../models/person-model";
import {AngularFirestore, AngularFirestoreCollection} from "@angular/fire/compat/firestore";

@Component({
  selector: 'app-item-detail-dialog',
  templateUrl: './item-detail-dialog.component.html',
  styleUrls: ['./item-detail-dialog.component.css']
})
export class ItemDetailDialogComponent implements OnInit, OnChanges {
  @Input() selectedItem: Item | null = null;
  newBidAmount: number = 0;
  bidders: Bids[] = [];
  creator: Person | null = null;

  private submissionForm: AngularFirestoreCollection<any> | null = null;

  constructor(private personService: PersonService, private firestore: AngularFirestore) {
  }

  ngOnInit(): void {
    this.submissionForm = this.firestore.collection('submissions');
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ((changes as any)['selectedItem'] && !(changes as any)['selectedItem'].firstChange) {
      // Logic to execute when selectedItem changes
      this.handleSelectedItemChange();
    }
  }

  private handleSelectedItemChange(): void {
    this.personService.getPerson(this.selectedItem!.sellerId).subscribe(person => this.creator = person)
    this.personService.getBidsOfItem(this.selectedItem!.id).subscribe(bids => this.bidders = bids)
    this.newBidAmount = 0;
  }

  placeBid(): void {
    console.log(this.selectedItem?.actualBid)
    if (this.newBidAmount > this.selectedItem!.actualBid) {
      this.personService.createBid(this.selectedItem?.id, this.newBidAmount).subscribe(bid => {
          this.selectedItem!.actualBid = this.newBidAmount
          this.handleSelectedItemChange()
          this.sendEmailToNotifyMembers()
        }
      )
    } else {
      alert('Your bid amount should be higher than the current bid.');
    }
  }

  private sendEmailToNotifyMembers() {
    var persons:string[] = []
    this.bidders.forEach(person => {
      if(person.itemId==this.selectedItem?.id && person.bidPrice<this.selectedItem.actualBid && !persons.includes(person.personOutgoingDto.username)){
        this.submissionForm?.add({
          email: 'lazardavid0906@gmail.com',
          name: person.personOutgoingDto.name,
          newBid: this.selectedItem?.actualBid,
          itemName: this.selectedItem?.name,
          url: 'http://localhost:4200/categories/'
            + this.selectedItem?.subCategoryOutgoingDto.categoryOutgoingDto.id +
            '/subcategories/' + this.selectedItem?.subCategoryOutgoingDto.id
        }).then(res => {
          console.log("Success")
        }).catch(err => console.log(err))
        persons.push(person.personOutgoingDto.username)
      }


    })
  }
}
