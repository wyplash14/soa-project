import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from "../../shared/services/category.service";
import {SubCategory} from "../../models/subCategory-model";
import {MenuItem} from "primeng/api";

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  subcategories: SubCategory[] = []
  menuItems: MenuItem[] = [];

  constructor(
    private route: ActivatedRoute,
    private router:Router,
    private categoryService: CategoryService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const itemId = params['id'];

      if (itemId) {
        this.loadSubcategories(itemId)
      }
    });

  }

  loadSubcategories(itemId: Number): void {
    this.categoryService.getSubcategories(itemId).subscribe((items) => {

      this.menuItems = items.map((item) => ({
        label: item.name,
        routerLink: [this.router.url+'/subcategories/', item.id]
      }));
    });
  }
}
