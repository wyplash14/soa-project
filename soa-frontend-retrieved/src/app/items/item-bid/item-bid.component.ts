import {Component, OnInit} from '@angular/core';
import {Item} from "../../models/item-model";
import {CategoryService} from "../../shared/services/category.service";
import {ActivatedRoute} from "@angular/router";
import {AngularFireStorage} from "@angular/fire/compat/storage";

@Component({
  selector: 'app-item-bid',
  templateUrl: './item-bid.component.html',
  styleUrls: ['./item-bid.component.css']
})
export class ItemBidComponent implements OnInit {
  url = "http://localhost:5057/micro-fe.js"
  items: Item[] = [];
  itemId: number = 0;
  displayDialog: boolean = false;
  selectedItem: Item | null = null;

  constructor(private route: ActivatedRoute, private itemService: CategoryService, private fs: AngularFireStorage,) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.itemId = params['id'];
      this.loadItems(this.itemId);
    });
  }

  loadItems(subcatId: number): void {
    this.itemService.getItems().subscribe(data => {
        data.forEach(item => {
          this.fs.ref("/files_item_" + item.id + "_" + item.name).getDownloadURL().subscribe(url => item.imageUrl = url)
        })
        this.items = data.filter(item => item.subCategoryOutgoingDto.id == subcatId)
      }
    )
  }

  showItemDetails(item: Item): void {
    this.itemService.getItemById(item.id).subscribe(item => {
      this.fs.ref("/files_item_" + item.id + "_" + item.name).getDownloadURL().subscribe(url => item.imageUrl= url)
      this.selectedItem = item
    })
    this.displayDialog = true;
  }
}
