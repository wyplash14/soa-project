package com.example.loggerservice;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class MessageConsumer {
    @KafkaListener(topics = "soa-project", groupId = "soa-project-id")
    public void listen(String message) {
        System.out.println("Received message: " + message);
    }

    @KafkaListener(topics = "soa-project-persons", groupId = "soa-project-id")
    public void listenMessage(String message) {
        System.out.println("Received message: " + message);
    }
}
