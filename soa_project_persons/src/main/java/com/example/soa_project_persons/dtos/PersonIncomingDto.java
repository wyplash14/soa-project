package com.example.soa_project_persons.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class PersonIncomingDto implements Serializable {
    private String name;

    private String username;

    private String email;

    private String phone;
}
