package com.example.soa_project_persons.dtos;

import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;

@Data
public class PersonBidIncomingDto implements Serializable {

    Long itemId;

    Long personId;

    Long bidPrice;
}
