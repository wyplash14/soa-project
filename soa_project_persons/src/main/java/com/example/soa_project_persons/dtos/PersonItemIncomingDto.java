package com.example.soa_project_persons.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class PersonItemIncomingDto implements Serializable {
    Long itemId;

    Long personId;
}
