package com.example.soa_project_persons.dtos;

import com.example.soa_project_persons.models.Person;
import lombok.Data;

import java.io.Serializable;

@Data
public class PersonItemOutgoingDto implements Serializable {
    Long itemId;

    PersonOutgoingDto personOutgoingDto;
}
