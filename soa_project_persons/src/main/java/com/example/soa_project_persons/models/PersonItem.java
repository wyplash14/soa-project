package com.example.soa_project_persons.models;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.*;

@Data
@Entity
@Getter
@Table(name = "PersonItems")
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PersonItem extends BaseEntity {

    Long itemId;

    @ManyToOne
    @JoinColumn(name = "persionId")
    Person person;
}
