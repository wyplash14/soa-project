package com.example.soa_project_persons.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

@Data
@Entity
@Getter
@Table(name = "Persons")
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Person extends BaseEntity {

    private String name;

    private String username;

    private String email;

    private String phone;
}
