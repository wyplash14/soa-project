package com.example.soa_project_persons.mappers;

import com.example.soa_project_persons.dtos.PersonBidIncomingDto;
import com.example.soa_project_persons.dtos.PersonBidOutgoingDto;
import com.example.soa_project_persons.models.PersonBids;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = PersonMapper.class)
public abstract class PersonBidMapper {
    public abstract PersonBids dtoToPersonBids(PersonBidIncomingDto personBidIncomingDto);

    @Mapping(source = "person", target = "personOutgoingDto")
    public abstract PersonBidOutgoingDto personBidToDto(PersonBids personBids);

    @IterableMapping(elementTargetType = PersonBidOutgoingDto.class)
    @Mapping(source = "person", target = "personOutgoingDto")
    public abstract List<PersonBidOutgoingDto> personBidOutgoingDtos(List<PersonBids> personBids);
}
