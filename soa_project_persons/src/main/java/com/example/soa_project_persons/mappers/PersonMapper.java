package com.example.soa_project_persons.mappers;

import com.example.soa_project_persons.dtos.PersonIncomingDto;
import com.example.soa_project_persons.dtos.PersonOutgoingDto;
import com.example.soa_project_persons.models.Person;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class PersonMapper {
    public abstract Person dtoToPerson(PersonIncomingDto personIncomingDto);

    public abstract PersonOutgoingDto personToDto(Person person);

    @IterableMapping(elementTargetType = PersonOutgoingDto.class)
    public abstract List<PersonOutgoingDto> personToDTOs(List<Person> personList);
}
