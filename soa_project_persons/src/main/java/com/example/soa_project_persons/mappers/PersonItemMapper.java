package com.example.soa_project_persons.mappers;

import com.example.soa_project_persons.dtos.PersonItemIncomingDto;
import com.example.soa_project_persons.dtos.PersonItemOutgoingDto;
import com.example.soa_project_persons.models.PersonItem;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = PersonMapper.class)
public abstract class PersonItemMapper {
    public abstract PersonItem dtoToPersonItem(PersonItemIncomingDto personItemIncomingDto);

    @Mapping(source = "person", target = "personOutgoingDto")
    public abstract PersonItemOutgoingDto personItemToDto(PersonItem personItem);

    @IterableMapping(elementTargetType = PersonItemOutgoingDto.class)
    @Mapping(source = "person", target = "personOutgoingDto")
    public abstract List<PersonItemIncomingDto> personItemToDTOs(List<PersonItem> personItemList);
}
