package com.example.soa_project_persons.repositories;

import com.example.soa_project_persons.models.PersonBids;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonBidRepository extends JpaRepository<PersonBids, Long> {

    List<PersonBids> findAllByItemId(Long itemId);
}
