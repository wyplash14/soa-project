package com.example.soa_project_persons.repositories;

import com.example.soa_project_persons.models.PersonItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonItemRepository extends JpaRepository<PersonItem, Long> {
    PersonItem findPersonItemByItemId(Long itemId);
}
