package com.example.soa_project_persons.repositories;

import com.example.soa_project_persons.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
