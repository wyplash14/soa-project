package com.example.soa_project_persons.controllers;

import com.example.soa_project_persons.controllers.exceptions.NotFoundException;
import com.example.soa_project_persons.dtos.PersonIncomingDto;
import com.example.soa_project_persons.dtos.PersonOutgoingDto;
import com.example.soa_project_persons.mappers.PersonMapper;
import com.example.soa_project_persons.models.Person;
import com.example.soa_project_persons.publisher.MessageProducer;
import com.example.soa_project_persons.services.PersonService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/persons")
public class PersonController {
    @Autowired
    private PersonService personService;

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private MessageProducer messageProducer;

    @GetMapping("/{id}")
    public PersonOutgoingDto getPerson(@PathVariable Long id) {
        Person person = personService.getPersonByPersonId(id);
        messageProducer.sendMessage("soa-project-persons", "A call was made on GET  /api/persons/" + id + " with result: " + person);

        if (person == null) throw new NotFoundException("Person with id = " + id + " does not exist.");
        return personMapper.personToDto(person);
    }

    @PostMapping
    public PersonOutgoingDto createPerson(@RequestBody @Valid PersonIncomingDto personIncomingDto) {
        Person person = personMapper.dtoToPerson(personIncomingDto);
        Person newItem = personService.createPerson(person);
        messageProducer.sendMessage("soa-project-persons", "A call was made on GET  /api/persons with result: " + newItem);

        return personMapper.personToDto(newItem);
    }
}
