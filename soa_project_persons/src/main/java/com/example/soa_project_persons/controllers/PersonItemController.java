package com.example.soa_project_persons.controllers;

import com.example.soa_project_persons.controllers.exceptions.NotFoundException;
import com.example.soa_project_persons.dtos.PersonItemIncomingDto;
import com.example.soa_project_persons.dtos.PersonItemOutgoingDto;
import com.example.soa_project_persons.mappers.PersonItemMapper;
import com.example.soa_project_persons.models.PersonItem;
import com.example.soa_project_persons.publisher.MessageProducer;
import com.example.soa_project_persons.services.PersonItemService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/persons/item")
public class PersonItemController {

    @Autowired
    private PersonItemService personItemService;

    @Autowired
    private PersonItemMapper personItemMapper;
    @Autowired
    private MessageProducer messageProducer;

    @GetMapping("/{id}")
    public PersonItemOutgoingDto getPersonItem(@PathVariable Long id) {
        PersonItem personItem = personItemService.getPersonItemOfItem(id);
        messageProducer.sendMessage("soa-project-persons", "A call was made on GET  /api/persons/item" + id + " with result: " + personItem);

        if (personItem == null) throw new NotFoundException("Person item with item id = " + id + " does not exist.");
        return personItemMapper.personItemToDto(personItem);
    }

    @PostMapping
    public PersonItemOutgoingDto createPersonItem(@RequestBody @Valid PersonItemIncomingDto personItemIncomingDto) {
        PersonItem personItem = personItemMapper.dtoToPersonItem(personItemIncomingDto);
        PersonItem newItem = personItemService.createPersonItem(personItem);
        messageProducer.sendMessage("soa-project-persons", "A call was made on POST  /api/persons/item with result: " + newItem);

        return personItemMapper.personItemToDto(newItem);
    }
}
