package com.example.soa_project_persons.controllers;

import com.example.soa_project_persons.controllers.exceptions.NotFoundException;
import com.example.soa_project_persons.dtos.PersonBidIncomingDto;
import com.example.soa_project_persons.dtos.PersonBidOutgoingDto;
import com.example.soa_project_persons.mappers.PersonBidMapper;
import com.example.soa_project_persons.models.PersonBids;
import com.example.soa_project_persons.publisher.MessageProducer;
import com.example.soa_project_persons.publisher.RabbitMQJsonProducer;
import com.example.soa_project_persons.publisher.RabbitMqProducer;
import com.example.soa_project_persons.services.PersonBidService;
import com.example.soa_project_persons.services.PersonService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/bids")
public class PersonBidController {

    @Autowired
    PersonBidService personBidService;

    @Autowired
    PersonService personService;

    @Autowired
    PersonBidMapper personBidMapper;

    @Autowired
    RabbitMQJsonProducer rabbitMqJsonProducer;

    @Autowired
    private MessageProducer messageProducer;

    @GetMapping("/{id}")
    public List<PersonBidOutgoingDto> getBidsOfItem(@PathVariable Long id) {
        List<PersonBids> personBids = personBidService.getBidsOfItem(id);
        messageProducer.sendMessage("soa-project-persons", "A call was made on GET  /api/bids/" + id + " with result: " + personBids);
        if (personBids == null) throw new NotFoundException("Item with id = " + id + " does not exist.");
        return personBidMapper.personBidOutgoingDtos(personBids);
    }

    @PostMapping
    public PersonBidOutgoingDto createBid(@RequestBody @Valid PersonBidIncomingDto personBidIncomingDto) {
        PersonBids personBids = personBidMapper.dtoToPersonBids(personBidIncomingDto);
        personBids.setPerson(personService.getPersonByPersonId(personBidIncomingDto.getPersonId()));
        PersonBids newItem = personBidService.createBid(personBids);
        rabbitMqJsonProducer.sendJsonMessage(newItem);
        messageProducer.sendMessage("soa-project-persons", "A call was made on POST  /api/bids with result: " + newItem);

        return personBidMapper.personBidToDto(newItem);
    }
}

