package com.example.soa_project_persons;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoaProjectPersonsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoaProjectPersonsApplication.class, args);
	}

}
