package com.example.soa_project_persons.services;

import com.example.soa_project_persons.models.PersonItem;
import com.example.soa_project_persons.repositories.PersonItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonItemService {

    @Autowired
    private PersonItemRepository personItemRepository;

    public PersonItem getPersonItemOfItem(Long itemId) {
        return personItemRepository.findPersonItemByItemId(itemId);
    }

    public PersonItem createPersonItem(PersonItem personItem) {
        return personItemRepository.save(personItem);
    }
}
