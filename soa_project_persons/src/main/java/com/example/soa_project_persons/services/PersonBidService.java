package com.example.soa_project_persons.services;

import com.example.soa_project_persons.models.PersonBids;
import com.example.soa_project_persons.repositories.PersonBidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonBidService {

    @Autowired
    PersonBidRepository personBidRepository;

    public List<PersonBids> getBidsOfItem(Long itemId) {
        return personBidRepository.findAllByItemId(itemId);
    }

    public PersonBids createBid(PersonBids personBids) {
        return personBidRepository.save(personBids);
    }
}
