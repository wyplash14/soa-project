package com.example.soa_project_persons.services;

import com.example.soa_project_persons.models.Person;
import com.example.soa_project_persons.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    @Autowired
    PersonRepository personRepository;

    public Person getPersonByPersonId(Long personId) {
        return personRepository.findById(personId).orElse(null);
    }

    public Person createPerson(Person person) {
        return personRepository.save(person);
    }
}
