package com.example.soa_project.mappers;

import com.example.soa_project.dtos.CategoryOutgoingDto;
import com.example.soa_project.dtos.SubCategoryIncomingDTO;
import com.example.soa_project.dtos.SubCategoryOutgoingDto;
import com.example.soa_project.models.Category;
import com.example.soa_project.models.SubCategory;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = CategoryMapper.class)
public abstract class SubCategoryMapper {
    public abstract SubCategory dtoToCategory(SubCategoryIncomingDTO subCategoryIncomingDTO);
    @Mapping(source = "category", target = "categoryOutgoingDto")
    public abstract SubCategoryOutgoingDto subCategoryToDTO(SubCategory subCategory);

    @IterableMapping(elementTargetType = CategoryOutgoingDto.class)
    @Mapping(source = "category", target = "categoryOutgoingDto")
    public abstract List<SubCategoryOutgoingDto> subCategoryToDTOs(List<SubCategory> categories);
}
