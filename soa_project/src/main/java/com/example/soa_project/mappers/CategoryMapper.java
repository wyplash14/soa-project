package com.example.soa_project.mappers;

import com.example.soa_project.dtos.*;
import com.example.soa_project.models.Category;
import com.example.soa_project.models.Item;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class CategoryMapper {
    public abstract Category dtoToCategory(CategoryIncomingDTO categoryIncomingDTO);

    public abstract CategoryOutgoingDto categoryToDTO(Category category);

    @IterableMapping(elementTargetType = CategoryOutgoingDto.class)
    public abstract List<CategoryOutgoingDto> categoryToDTOs(List<Category> categories);
}