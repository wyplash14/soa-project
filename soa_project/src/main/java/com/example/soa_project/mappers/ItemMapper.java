package com.example.soa_project.mappers;

import com.example.soa_project.dtos.DetailedItemOutgoingDto;
import com.example.soa_project.dtos.ItemIncomingDTO;
import com.example.soa_project.dtos.ItemOutgoingDTO;
import com.example.soa_project.models.Item;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = SubCategoryMapper.class)
public abstract class ItemMapper {
    public abstract Item dtoToItem(ItemIncomingDTO itemIncomingDTO);

    @Mapping(source = "subCategory", target = "subCategoryOutgoingDto")
    public abstract ItemOutgoingDTO itemToDTO(Item item);

    @Mapping(source = "subCategory", target = "subCategoryOutgoingDto")
    public abstract DetailedItemOutgoingDto itemToDetailedDTO(Item item);

    @IterableMapping(elementTargetType = ItemOutgoingDTO.class)
    @Mapping(source = "subCategory", target = "subCategoryOutgoingDto")
    public abstract List<ItemOutgoingDTO> itemToDTOs(List<Item> items);
}
