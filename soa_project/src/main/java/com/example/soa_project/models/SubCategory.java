package com.example.soa_project.models;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@Table(name = "SubCategory")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SubCategory extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "categoryId")
    private Category category;

    private String name;

}
