package com.example.soa_project.models;

import jakarta.persistence.*;
import lombok.*;

import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Getter
@Table(name = "Item")
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Item extends BaseEntity {
    private String name;
    private Long estimatedValue;
    private Long minimalValue;
    private Long actualBid;
    private Timestamp endOfAuction;

    public String description;
    public Long sellerId;
    @ManyToOne
    @JoinColumn(name = "subCategoryId")
    public SubCategory subCategory;
}
