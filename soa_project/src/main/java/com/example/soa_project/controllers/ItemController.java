package com.example.soa_project.controllers;

import com.example.soa_project.controllers.exceptions.NotFoundException;
import com.example.soa_project.dtos.DetailedItemOutgoingDto;
import com.example.soa_project.dtos.ItemIncomingDTO;
import com.example.soa_project.dtos.ItemOutgoingDTO;
import com.example.soa_project.mappers.ItemMapper;
import com.example.soa_project.models.Item;
import com.example.soa_project.producer.MessageProducer;
import com.example.soa_project.services.ItemService;
import com.example.soa_project.services.SubCategoryService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/api/items")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private ItemMapper itemMapper;

    @Autowired
    private SubCategoryService subCategoryService;
    @Autowired
    private MessageProducer messageProducer;
    @GetMapping
    public List<ItemOutgoingDTO> getItems() {
        List<Item> items = itemService.findAll();
        return itemMapper.itemToDTOs(items);
    }

    @GetMapping("/{id}")
    public DetailedItemOutgoingDto getItem(@PathVariable Long id) {
        Item item = itemService.findById(id);
        messageProducer.sendMessage("soa-project", "A call was made on GET /api/items with result: " + item);
        if (item == null) throw new NotFoundException("Item with id = " + id + " does not exist.");
        return itemMapper.itemToDetailedDTO(item);
    }

    @PostMapping
    public DetailedItemOutgoingDto createItem(@RequestBody @Valid ItemIncomingDTO itemIncomingDTO) {
        if (Objects.isNull(subCategoryService.findById(itemIncomingDTO.getSubCategoryId()))) {
            log.error("Subcategory with id = {} does not exist.", itemIncomingDTO.getSubCategoryId());
            throw new NotFoundException("Subcategory with id = " + itemIncomingDTO.getSubCategoryId() + " does not exist.");
        }

        Item item = itemMapper.dtoToItem(itemIncomingDTO);
        item.setSubCategory(subCategoryService.findById(itemIncomingDTO.getSubCategoryId()));
        Item newItem = itemService.create(item);
        messageProducer.sendMessage("soa-project", "A call was made on POST /api/items with result: " + newItem);
        return itemMapper.itemToDetailedDTO(newItem);
    }
}
