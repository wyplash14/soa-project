package com.example.soa_project.controllers;

import com.example.soa_project.controllers.exceptions.NotFoundException;
import com.example.soa_project.dtos.SubCategoryIncomingDTO;
import com.example.soa_project.dtos.SubCategoryOutgoingDto;
import com.example.soa_project.mappers.SubCategoryMapper;
import com.example.soa_project.models.Category;
import com.example.soa_project.models.SubCategory;
import com.example.soa_project.producer.MessageProducer;
import com.example.soa_project.services.CategoryService;
import com.example.soa_project.services.SubCategoryService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/subcategories")
public class SubCategoryController {

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubCategoryMapper subCategoryMapper;
    @Autowired
    private MessageProducer messageProducer;
    @GetMapping("/category/{categoryId}")
    public List<SubCategoryOutgoingDto> getSubCategories(@PathVariable Long categoryId) {
        Category category = categoryService.findById(categoryId);
        if (category != null) {
            List<SubCategory> subCategories = subCategoryService.findAllByCategory(categoryId);
            messageProducer.sendMessage("soa-project", "A call was made on GET /api/subcategories/category/+" + categoryId + " with result: " + subCategories);
            return subCategoryMapper.subCategoryToDTOs(subCategories);
        } else {
            throw new NotFoundException("Category with id = " + categoryId + " does not exist.");
        }
    }

    @GetMapping("/{id}")
    public SubCategoryOutgoingDto getSubCategory(@PathVariable Long id) {
        SubCategory category = subCategoryService.findById(id);
        messageProducer.sendMessage("soa-project", "A call was made on GET /api/subcategories/" + id + " with result: " + category);
        if (category == null) throw new NotFoundException("Sub-Category with id = " + id + " does not exist.");
        return subCategoryMapper.subCategoryToDTO(category);
    }

    @PostMapping
    public SubCategoryOutgoingDto createCategory(@RequestBody @Valid SubCategoryIncomingDTO subCategoryIncomingDTO) {
        Category category = categoryService.findById(subCategoryIncomingDTO.getCategoryId());

        if (category == null) {
            throw new NotFoundException("Sub-Category with id = " + subCategoryIncomingDTO.getCategoryId() + " does not exist.");
        }

        SubCategory subCategory = subCategoryMapper.dtoToCategory(subCategoryIncomingDTO);
        subCategory.setCategory(category);
        SubCategory newItem = subCategoryService.create(subCategory);
        newItem.setCategory(category);
        messageProducer.sendMessage("soa-project", "A call was made on POST /api/subcategories with result: " + newItem);

        return subCategoryMapper.subCategoryToDTO(newItem);
    }
}
