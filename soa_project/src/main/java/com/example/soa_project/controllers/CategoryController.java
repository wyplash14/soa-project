package com.example.soa_project.controllers;

import com.example.soa_project.controllers.exceptions.NotFoundException;
import com.example.soa_project.dtos.CategoryIncomingDTO;
import com.example.soa_project.dtos.CategoryOutgoingDto;
import com.example.soa_project.mappers.CategoryMapper;
import com.example.soa_project.models.Category;
import com.example.soa_project.producer.MessageProducer;
import com.example.soa_project.services.CategoryService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/categories")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryMapper categoryMapper;

    @GetMapping
    public List<CategoryOutgoingDto> getCategories() {
        List<Category> categories = categoryService.findAll();
        messageProducer.sendMessage("soa-project", "A call was made on GET  /api/categories with result: " + categories);
        return categoryMapper.categoryToDTOs(categories);
    }


    @Autowired
    private MessageProducer messageProducer;

    @GetMapping("/{id}")
    public CategoryOutgoingDto getCategory(@PathVariable Long id) {
        Category category = categoryService.findById(id);
        messageProducer.sendMessage("soa-project", "A call was made on GET  /api/categories/" + id + " with result: " + category);
        if (category == null) throw new NotFoundException("Category with id = " + id + " does not exist.");
        return categoryMapper.categoryToDTO(category);
    }

    @PostMapping
    public CategoryOutgoingDto createCategory(@RequestBody @Valid CategoryIncomingDTO categoryIncomingDTO) {
        Category category = categoryMapper.dtoToCategory(categoryIncomingDTO);
        Category newItem = categoryService.create(category);
        messageProducer.sendMessage("soa-project", "A call was made on POST /api/categories with result: " + newItem);
        return categoryMapper.categoryToDTO(newItem);
    }
}
