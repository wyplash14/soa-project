package com.example.soa_project.services;

import com.example.soa_project.models.Item;
import com.example.soa_project.repositories.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {
    @Autowired
    private ItemRepository itemRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemService.class);

    public List<Item> findAll() {
        return itemRepository.findAll();
    }

    public Item create(Item item) {
        return itemRepository.save(item);
    }

    public Item findById(Long id) {
        return itemRepository.findById(id).orElse(null);
    }

    public void update(Item item) {
        Item updatedItem = itemRepository.findById(item.getId()).orElse(null);
        if (updatedItem != null) {
            updatedItem.setActualBid(item.getActualBid());
            itemRepository.saveAndFlush(updatedItem);
        }
    }

    public void delete(Item item) {
        itemRepository.findById(item.getId()).ifPresent(itemToDelete -> itemRepository.save(itemToDelete));
    }

}
