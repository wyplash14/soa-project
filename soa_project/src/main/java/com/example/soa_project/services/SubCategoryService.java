package com.example.soa_project.services;

import com.example.soa_project.models.Category;
import com.example.soa_project.models.SubCategory;
import com.example.soa_project.repositories.CategoryRepository;
import com.example.soa_project.repositories.SubCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubCategoryService {
    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    public List<SubCategory> findAllByCategory(Long id) {
        Category category = categoryRepository.findById(id).orElse(null);
        if (categoryRepository != null) {
            return subCategoryRepository.findAllByCategory(category);
        }
        return null;
    }

    public SubCategory findById(Long id) {
        return subCategoryRepository.findById(id).orElse(null);
    }

    public SubCategory create(SubCategory subCategory) {
        return subCategoryRepository.save(subCategory);
    }


    public void update(SubCategory subCategory) {
        subCategoryRepository.findById(subCategory.getId()).ifPresent(itemToUpdate -> subCategoryRepository.save(itemToUpdate));
    }

    public void delete(SubCategory item) {
        subCategoryRepository.findById(item.getId()).ifPresent(itemToDelete -> subCategoryRepository.save(itemToDelete));
    }
}
