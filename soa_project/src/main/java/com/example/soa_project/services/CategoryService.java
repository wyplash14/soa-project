package com.example.soa_project.services;

import com.example.soa_project.models.Category;
import com.example.soa_project.models.Item;
import com.example.soa_project.repositories.CategoryRepository;
import com.example.soa_project.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    public Category create(Category category) { return categoryRepository.save(category); }

    public Category findById(Long id) {
        return categoryRepository.findById(id).orElse(null);
    }

    public void update(Category item) {
        categoryRepository.findById(item.getId()).ifPresent(itemToUpdate -> categoryRepository.save(itemToUpdate));
    }

    public void delete(Category item) {
        categoryRepository.findById(item.getId()).ifPresent(itemToDelete -> categoryRepository.save(itemToDelete));
    }
}
