package com.example.soa_project.repositories;

import com.example.soa_project.models.Category;
import com.example.soa_project.models.SubCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubCategoryRepository extends JpaRepository<SubCategory, Long> {

    List<SubCategory> findAllByCategory(Category category);
}
