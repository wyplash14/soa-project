package com.example.soa_project.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

@Data
@EqualsAndHashCode
@ToString
public class ItemOutgoingDTO implements Serializable {
    private Long id;
    private String name;
    private Long estimatedValue;
    private Long minimalValue;
    private Long actualBid;
    private Timestamp endOfAuction;
    private SubCategoryOutgoingDto subCategoryOutgoingDto;
}
