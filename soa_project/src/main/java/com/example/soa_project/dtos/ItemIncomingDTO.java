package com.example.soa_project.dtos;

import com.example.soa_project.models.SubCategory;
import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class ItemIncomingDTO implements Serializable {
    @NonNull
    private String name;

    @NonNull
    private Long estimatedValue;
    private Long minimalValue;
    private Long actualBid;

    @NonNull
    private Timestamp endOfAuction;

    public String description;

    @NonNull
    public Long sellerId;

    @NonNull
    public Long subCategoryId;
}
