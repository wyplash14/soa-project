package com.example.soa_project.dtos;

import com.example.soa_project.models.Category;
import lombok.Data;

import java.io.Serializable;

@Data
public class SubCategoryOutgoingDto implements Serializable {
    private Long id;
    private CategoryOutgoingDto categoryOutgoingDto;

    private String name;
}
