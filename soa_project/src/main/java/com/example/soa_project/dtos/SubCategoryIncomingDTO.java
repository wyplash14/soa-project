package com.example.soa_project.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class SubCategoryIncomingDTO implements Serializable {
    private Long categoryId;

    private String name;
}
