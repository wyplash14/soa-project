package com.example.soa_project.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class CategoryIncomingDTO implements Serializable {
    private String name;
}
