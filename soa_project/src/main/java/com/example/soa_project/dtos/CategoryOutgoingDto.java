package com.example.soa_project.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class CategoryOutgoingDto implements Serializable {
    private String name;

    private Long id;
}
