package com.example.soa_project.dtos;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class DetailedItemOutgoingDto implements Serializable {
    private Long id;
    private String name;
    private Long estimatedValue;
    private Long minimalValue;
    private Long actualBid;
    private Timestamp endOfAuction;
    public String description;
    public Long sellerId;
    private SubCategoryOutgoingDto subCategoryOutgoingDto;
}
