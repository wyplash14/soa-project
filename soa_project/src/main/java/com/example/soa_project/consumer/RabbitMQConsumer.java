package com.example.soa_project.consumer;

import com.example.soa_project.models.Item;
import com.example.soa_project.models.PersonBids;
import com.example.soa_project.services.ItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQConsumer {

    @Autowired
    ItemService itemService;

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQConsumer.class);

    @RabbitListener(queues = {"${rabbitmq.queue.name}"})
    public void consume(String message) {
        LOGGER.info(String.format("Recived message -> %s", message));
    }

    @RabbitListener(queues = {"${rabbitmq.queue.json.name}"})
    public void consumeJson(PersonBids personBids) {
        LOGGER.info(String.format("Received message -> %s", personBids));
        Item item = itemService.findById(personBids.getItemId());
        LOGGER.info(item.toString());
        item.setActualBid(personBids.getBidPrice());
        itemService.update(item);
    }
}
